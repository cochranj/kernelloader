## KernelLoader.inf
 #  Loads and runs a Linux kernel like the EFI shell.
 #  The firmware of my ThinkPad T450s does not like to run the Linux kernel 
 #  directly, even though it is properly configured as an EFI stub kernel.  The
 #  same kernel will run as an EFI binary when invoked from the TianoCore shell.
 #  This program will load the kernel in the same manner, but quickly, and 
 #  without the 5 second delay caused by Startup.nsh.
 #  
 #  Copyright (C) 2019, Jack Cochran
 #  
 #  Redistribution and use in source and binary forms, with or without 
 #  modification, are permitted provided that the following conditions are met:
 #  
 #  * Redistributions of source code must retain the above copyright
 #    notice, this list of conditions and the following disclaimer.
 #  * Redistributions in binary form must reproduce the above copyright notice,
 #    this list of conditions and the following disclaimer in the documentation
 # 	and/or other materials provided with the distribution.
 # 
 #  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR
 #  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 #  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
 #  EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT, INDIRECT, 
 #  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 #  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA 
 #  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 #  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
 #  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 #  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 # 
##

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = KernelLoader
  FILE_GUID                      = EE7D8352-7242-4940-805D-C8E2636C8050
  MODULE_TYPE                    = UEFI_APPLICATION
  VERSION_STRING                 = 1.0
  ENTRY_POINT                    = UefiMain
#
# The following information is for reference only and not required by the build 
# tools.
#
#  VALID_ARCHITECTURES           = IA32 X64 IPF EBC Etc...
#

[Sources]
  KernelLoader.c

[Packages]
  MdePkg/MdePkg.dec
  ShellPkg/ShellPkg.dec
  
[LibraryClasses]
  UefiApplicationEntryPoint
  UefiLib
  TimerLib
  UefiShellLib
  
[Guids]

[Ppis]

[Protocols]

[FeaturePcd]

[Pcd]

