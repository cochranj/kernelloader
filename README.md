# KernelLoader

KernelLoader is an EFI binary application which loads another EFI application by
searching for a particular GPT parition GUID (for a particular EFI System 
Partition), and by finding a particular EFI executable file on that partition.

The motivation is that I bought a laptop, but the UEFI firmware refuses to boot
an EFI-stub Linux kernel directly.  However, the firmware boots this UEFI 
application just fine, and this application is then able to boot the kernel.

This short program demonstrates a few techniques, including enumerating all 
filesystem devices, traversing from a filesystem device handle to obtain a media
access device path, reading the parition GUID off of the device path, and loading
and starting an EFI image.

## Building

This project is meant to be compiled with the EFI Development Kit (EDK) II.  

  1. Get or clone [EDK II](https://github.com/tianocore/edk2).  I worked off of
     the "edk2-stable201811" branch.
  2. Add these files as a subfolder within that tree.
  3. Follow the [directions](https://github.com/tianocore/tianocore.github.io/wiki/Getting-Started-with-EDK-II) for setting up the build environment.  For me this 
     involved.
    
  * Compiling the base tools per instructions.
  * Modifying large parts of `Conf/tools_def.txt` to support GCC 7.
  * Configuring `Conf/target.txt` to compile the `KernelLoader/KernelLoader.dsc` package, with the GCC7 toolchain, and X64 architecture.
  * I was able to compile IA32 binaries, but my firmware can't run them.

## Copying

This software is distributed under the following terms.

  Copyright (C) 2019, Jack Cochran

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR
  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
  EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT, INDIRECT,
  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA
  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


