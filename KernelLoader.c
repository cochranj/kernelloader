/* KernelLoader.c
 *  Loads and runs a Linux kernel like the EFI shell.
 *  The firmware of my ThinkPad T450s does not like to run the Linux kernel 
 *  directly, even though it is properly configured as an EFI stub kernel.  The
 *  same kernel will run as an EFI binary when invoked from the TianoCore shell.
 *  This program will load the kernel in the same manner, but quickly, and 
 *  without the 5 second delay caused by Startup.nsh.
 *  
 *  Copyright (C) 2019, Jack Cochran
 *  
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions are met:
 *  
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 * 	and/or other materials provided with the distribution.
 * 
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR
 *  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
 *  EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT, INDIRECT, 
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA 
 *  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 *  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <Uefi.h>
#include <Library/UefiApplicationEntryPoint.h>
#include <Library/UefiLib.h>
#include <Library/DevicePathLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/TimerLib.h>
#include <Protocol/SimpleFileSystem.h>


EFI_STATUS LocateFileSystem(EFI_GUID* file_partition_guid,
                            EFI_HANDLE* filesystem_out,
                            EFI_HANDLE ImageHandle,
							EFI_SYSTEM_TABLE* SystemTable);
EFI_STATUS FilesystemMatchesGuid(EFI_HANDLE filesystem,
								 EFI_GUID* guid_to_find,
							     BOOLEAN* result,
							     EFI_HANDLE ImageHandle,
								 EFI_SYSTEM_TABLE* SystemTable);
EFI_STATUS WalkToPartitionDevicePath
	(CONST EFI_DEVICE_PATH_PROTOCOL* device_path_in,
	 CONST EFI_DEVICE_PATH_PROTOCOL** partition_path_out,
	 EFI_HANDLE ImageHandle,
	 EFI_SYSTEM_TABLE* SystemTable);
EFI_STATUS DevicePathMatchesGuid(CONST EFI_DEVICE_PATH_PROTOCOL* device_path,
                                 EFI_GUID* guid_to_check,
								 BOOLEAN* result);
EFI_STATUS GuidOfDevicePath(CONST EFI_DEVICE_PATH_PROTOCOL* device_path,
                            EFI_GUID* guid_out);
BOOLEAN IsHardDrive(CONST EFI_DEVICE_PATH_PROTOCOL* device_path);
VOID PrintDevicePath(CONST EFI_DEVICE_PATH_PROTOCOL* device_path);
VOID PrintGuid(EFI_GUID* guid);

EFI_STATUS GetFileDevicePathInFilesystem
	(EFI_HANDLE filesystem,
	 CHAR16* path_str,
	 EFI_DEVICE_PATH_PROTOCOL** device_path_out,
	 EFI_HANDLE ImageHandle,
	 EFI_SYSTEM_TABLE* SystemTable);
EFI_STATUS OpenFileByPath(EFI_HANDLE filesystem,
                          CHAR16* path_str,
						  EFI_FILE_PROTOCOL** file_out,
						  EFI_HANDLE ImageHandle,
						  EFI_SYSTEM_TABLE* SystemTable);

EFI_STATUS BootImage(EFI_DEVICE_PATH_PROTOCOL* image_device_path,
                     EFI_HANDLE ImageHandle,
					 EFI_SYSTEM_TABLE* SystemTable);
/**
  as the real entry point for the application.

  @param[in] ImageHandle    The firmware allocated handle for the EFI image.  
  @param[in] SystemTable    A pointer to the EFI System Table.
  
  @retval EFI_SUCCESS       The entry point is executed successfully.
  @retval other             Some error occurs when executing this entry point.

**/
EFI_STATUS
EFIAPI
UefiMain (
  IN EFI_HANDLE        ImageHandle,
  IN EFI_SYSTEM_TABLE  *SystemTable
  )
{
	EFI_STATUS status = EFI_SUCCESS;
	EFI_HANDLE filesystem = NULL;
	EFI_DEVICE_PATH_PROTOCOL* image_file_path = NULL;
	
	/* Replace this with your own GUID from your EFI System Partition (!!) */
	EFI_GUID filesystem_guid = {
		0xb9760d82,
		0xf3d5,
		0x48de,
		{0x84, 0x13, 0xd3, 0x45, 0xe7, 0xd3, 0xcc, 0x0c}
	};

	/* This is a default path for booting.  It can be replaced with a path to
	 * another EFI binary.
	 */
	CHAR16 boot_path[] = L".\\EFI\\Boot\\bootx64.efi";

	Print(L"Looking for filesystem matching GUID:  ");
	PrintGuid(&filesystem_guid);

	status = LocateFileSystem
		(&filesystem_guid,
	 	 &filesystem,
		 ImageHandle,
		 SystemTable);

	if ((status != EFI_SUCCESS) || !filesystem) {
		Print(L"Unable to find matching filesystem.  Error %d. Abort.\n", 
		      status);
		goto cleanup;
	}
	
	Print(L"Found matching filesystem.  Looking for path: %s.\n",
	      boot_path);

	status = GetFileDevicePathInFilesystem
		(filesystem,
		 boot_path,
		 &image_file_path,
		 ImageHandle,
		 SystemTable);
	if (status != EFI_SUCCESS) {
		Print(L"Image file failed to get path.  Error %d. Abort.\n",
		      status);
		goto cleanup;
	}

	Print(L"Booting the image...\n");
	status = BootImage(image_file_path,
	                   ImageHandle,
					   SystemTable);

cleanup:
	MicroSecondDelay(10000000); /* 10 seconds */
	return status;
}

EFI_STATUS LocateFileSystem(EFI_GUID* file_partition_guid,
                            EFI_HANDLE* filesystem_out,
                            EFI_HANDLE ImageHandle,
							EFI_SYSTEM_TABLE* SystemTable) {

	EFI_STATUS status = EFI_SUCCESS;
	
	EFI_GUID simple_filesystem_guid = EFI_SIMPLE_FILE_SYSTEM_PROTOCOL_GUID;
	UINTN num_filesystems = 0;
	EFI_HANDLE* filesystems = NULL;

	*filesystem_out = NULL;

	status = SystemTable->BootServices->LocateHandleBuffer
		(ByProtocol,
		 &simple_filesystem_guid,
		 NULL,
		 &num_filesystems,
		 &filesystems);
	if (status != EFI_SUCCESS) {
		return status;
	}

	for (UINTN i = 0; i < num_filesystems; ++i) {
		BOOLEAN is_match = FALSE;

		status = FilesystemMatchesGuid
			(filesystems[i],
			 file_partition_guid,
			 &is_match,
			 ImageHandle,
			 SystemTable);
		if ((status == EFI_SUCCESS) && is_match) {
			*filesystem_out = filesystems[i];
			break;
		}
	}
	FreePool(filesystems);

	if ((status == EFI_SUCCESS) && !(*filesystem_out)) {
		status = EFI_NOT_FOUND;
	}	
	return status;
}


EFI_STATUS FilesystemMatchesGuid(EFI_HANDLE filesystem,
                                 EFI_GUID* guid_to_find,
							     BOOLEAN* result,
							     EFI_HANDLE ImageHandle,
							     EFI_SYSTEM_TABLE* SystemTable) {

	EFI_GUID device_path_guid = EFI_DEVICE_PATH_PROTOCOL_GUID;
	EFI_STATUS status = EFI_SUCCESS;	
	EFI_DEVICE_PATH_PROTOCOL* filesystem_device_path = NULL;
	CONST EFI_DEVICE_PATH_PROTOCOL* partition_device_path = NULL;
	BOOLEAN is_match = FALSE;

	status = SystemTable->BootServices->OpenProtocol
		(filesystem,
		 &device_path_guid,
		 (VOID**) &filesystem_device_path,
		 ImageHandle,
		 NULL,
		 EFI_OPEN_PROTOCOL_GET_PROTOCOL);
	if (status != EFI_SUCCESS) {
		Print(L"Unable to get device path for block I/O %x.\n", 
		      (VOID*) filesystem);
		return status;
	}

	status = WalkToPartitionDevicePath
		(filesystem_device_path,
		 &partition_device_path,
		 ImageHandle,
		 SystemTable);
	if (status != EFI_SUCCESS) {
		Print(L"Unable to find a harddrive device path for block I/O %x.\n",
		      (VOID*) filesystem);
		goto cleanup;
	}

	PrintDevicePath(filesystem_device_path);
	PrintDevicePath(partition_device_path);

	status = DevicePathMatchesGuid
		(partition_device_path,
		 guid_to_find,
		 &is_match);
	if (status == EFI_SUCCESS) {
		*result = is_match;
	}

cleanup:
	SystemTable->BootServices->CloseProtocol
		(filesystem_device_path,
		 &device_path_guid,
		 ImageHandle,
		 NULL);
	return status;
}


EFI_STATUS WalkToPartitionDevicePath
	(CONST EFI_DEVICE_PATH_PROTOCOL* device_path_in,
	 CONST EFI_DEVICE_PATH_PROTOCOL** partition_path_out,
	 EFI_HANDLE ImageHandle,
	 EFI_SYSTEM_TABLE* SystemTable) {

	*partition_path_out = NULL;

	while (!IsDevicePathEndType(device_path_in)) {
		if (IsHardDrive(device_path_in)) {
			*partition_path_out = device_path_in;
		}
		device_path_in = NextDevicePathNode(device_path_in);
	}

	return (*partition_path_out != NULL) ? EFI_SUCCESS : EFI_NOT_FOUND;
}


VOID PrintDevicePath(CONST EFI_DEVICE_PATH_PROTOCOL* device_path) {
	CHAR16* device_path_str = NULL;

	device_path_str = ConvertDevicePathToText(device_path, TRUE, FALSE);

	if (device_path_str) {
		Print(L"Device path:  %s\n", device_path_str);
		FreePool(device_path_str);
	} else {
		Print(L"Unable to get device path string.\n");
	}
}

EFI_STATUS GuidOfDevicePath(CONST EFI_DEVICE_PATH_PROTOCOL* device_path,
                            EFI_GUID* guid_out) {

	EFI_STATUS status = EFI_SUCCESS;
	HARDDRIVE_DEVICE_PATH* harddrive = NULL;

	/*
	Print(L"GuidOfDevicePath: type is %d.\n", device_path->Type);
	Print(L"GuidOfDevicePath: SubType is %d.\n", device_path->SubType);
	*/
	if (!IsHardDrive(device_path)) {
		Print(L"GuidOfDevicePath: Device is not a hard drive.\n");
		return EFI_INVALID_PARAMETER;
	}

	harddrive = (HARDDRIVE_DEVICE_PATH*) device_path;


	CopyGuid((GUID*) guid_out,
	         (CONST GUID*) harddrive->Signature);
	return status;
}

BOOLEAN IsHardDrive(CONST EFI_DEVICE_PATH_PROTOCOL* device_path) {
	return (device_path->Type == MEDIA_DEVICE_PATH) && 
	       (device_path->SubType == MEDIA_HARDDRIVE_DP); /* Hard Drive path */
}

EFI_STATUS DevicePathMatchesGuid(CONST EFI_DEVICE_PATH_PROTOCOL* device_path,
                                 EFI_GUID* guid_to_check,
								 BOOLEAN* result) {

	EFI_STATUS status = EFI_SUCCESS;
	EFI_GUID guid = {0};
	
	status = GuidOfDevicePath(device_path, &guid);
	if (status != EFI_SUCCESS) {
		*result = 0;
		return status;
	}

	Print(L"Found Device Guid:\n");
	PrintGuid(&guid);

	*result = CompareGuid((CONST GUID*) &guid, 
	                      (CONST GUID*) guid_to_check);
	return status;
}

VOID PrintGuid(EFI_GUID* guid) {
	Print(L"%x-%x-%x-%x%x-%x%x%x%x%x%x\n", guid->Data1, 
	                                       guid->Data2, 
										   guid->Data3,
										   guid->Data4[0],
										   guid->Data4[1],
										   guid->Data4[2],
										   guid->Data4[3],
										   guid->Data4[4],
										   guid->Data4[5],
										   guid->Data4[6],
										   guid->Data4[7]);
}

EFI_STATUS GetFileDevicePathInFilesystem
	(EFI_HANDLE filesystem,
	 CHAR16* path_str,
	 EFI_DEVICE_PATH_PROTOCOL** device_path_out,
	 EFI_HANDLE ImageHandle,
	 EFI_SYSTEM_TABLE* SystemTable) {

	EFI_STATUS status = EFI_SUCCESS;
	*device_path_out = FileDevicePath
		(filesystem,
		 path_str);

	if (!(*device_path_out)) {
		status = EFI_INVALID_PARAMETER;
	}
	return status;
}
	
EFI_STATUS OpenFileByPath(EFI_HANDLE filesystem,
                          CHAR16* path_str,
						  EFI_FILE_PROTOCOL** file_out,
						  EFI_HANDLE ImageHandle,
						  EFI_SYSTEM_TABLE* SystemTable) {

	EFI_STATUS status = EFI_SUCCESS;
	EFI_GUID simple_filesystem_guid = EFI_SIMPLE_FILE_SYSTEM_PROTOCOL_GUID;
	EFI_SIMPLE_FILE_SYSTEM_PROTOCOL* filesystem_proto = NULL;
	EFI_FILE_PROTOCOL* root_dir = NULL;

	status = SystemTable->BootServices->OpenProtocol
		(filesystem,
		 &simple_filesystem_guid,
		 (VOID**) &filesystem_proto,
		 ImageHandle,
		 NULL,
		 EFI_OPEN_PROTOCOL_GET_PROTOCOL);
	if (status != EFI_SUCCESS) {
		Print(L"OpenFileByPath: Unable to open simple filesystem. "
		      L"Error code %d.\n",
		      status);
		return status;
	}

	status = filesystem_proto->OpenVolume
		(filesystem_proto,
		 &root_dir);
	if (status != EFI_SUCCESS) {
		Print(L"OpenFileByPath: Unable to open volume. Error code %d.\n",
		      status);
		goto cleanup_proto;
	}

	status = root_dir->Open
		(root_dir,
		 file_out,
		 path_str,
		 EFI_FILE_MODE_READ | EFI_FILE_MODE_WRITE,
		 0);
	if (status != EFI_SUCCESS) {
		Print(L"OpenFileByPath: Unable to open file. Error code %d.\n",
			  status);
		goto cleanup_volume;
	}

cleanup_volume:
	status = root_dir->Close(root_dir);

cleanup_proto:
	status = SystemTable->BootServices->CloseProtocol
		(filesystem,
		 &simple_filesystem_guid,
		 ImageHandle,
		 NULL);
	return status;
}


EFI_STATUS BootImage(EFI_DEVICE_PATH_PROTOCOL* image_device_path,
                     EFI_HANDLE ImageHandle,
					 EFI_SYSTEM_TABLE* SystemTable) {

	EFI_STATUS status = EFI_SUCCESS;
	EFI_HANDLE boot_image = NULL;

	status = SystemTable->BootServices->LoadImage
		(FALSE,
		 ImageHandle,
		 image_device_path,
		 NULL,
		 0,
		 &boot_image);
	if (status != EFI_SUCCESS) {
		Print(L"BootImage:  Unable to load image.  Code %d.\n", status);
		return status;
	}

	status = SystemTable->BootServices->StartImage
		(boot_image,
		 NULL,
		 NULL);
	return status;
}
		 




